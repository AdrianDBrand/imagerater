﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageRater.Data
{
    public class ImageRaterDbContext : DbContext
    {
        public ImageRaterDbContext()
            : base("ImageRatingConnection")
        {
            Database.SetInitializer<ImageRaterDbContext>(new DropCreateDatabaseIfModelChanges<ImageRaterDbContext>());
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Image> Images { get; set; }

        public DbSet<ImageRating> ImageRatings { get; set; }
    }
}
