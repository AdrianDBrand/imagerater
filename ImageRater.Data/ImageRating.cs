﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageRater.Data
{
    public class ImageRating
    {
        [Key, Column(Order = 0)]
        public int ImageId { get; set; }

        [Key, Column(Order = 1)]
        public int UserId { get; set; }

        public bool Liked { get; set; }

        public Image Image { get; set; }
    }
}
