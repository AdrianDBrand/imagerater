﻿(function () {
    angular.module('app')
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider
                .state('rated', {
                    url: '/rated',
                    templateUrl: 'app/rated/Rated-View.html',
                    controller: 'ratedController as vm',
                    data: { pageTitle: 'Rated Images' }
                });
        }]);
})();