﻿(function () {
    angular.module('app')
        .controller('ratedController', ['appData', 'userService', '$state', function (appData, userService, $state) {
            var vm = this;

            userService.getRatings(appData.user.id).then(function (ratings) {
                vm.ratings = ratings;
            });
        }]);
})();