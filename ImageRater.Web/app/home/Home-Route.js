﻿(function () {
    angular.module('app')
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider
                .state('home', {
                    url: '/',
                    templateUrl: 'app/home/Home-View.html',
                    controller: 'homeController as vm'
                });
        }]);
})();