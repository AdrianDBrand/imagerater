﻿(function () {
    angular.module('app')
        .controller('homeController', ['appData', 'imagesService', function (appData, imagesService) {
            var vm = this;
            
            getRandomImage();

            vm.yes = function () {
                rate(true);
            };

            vm.no = function () {
                rate(false);
            };

            function rate(liked) {
                var image = vm.image;
                vm.image = null;
                imagesService.rate({ imageId: image.id, userId: appData.user.id, liked: liked }).then(function () {
                    getRandomImage();
                });
            }

            function getRandomImage() {
                imagesService.getRandom().then(function (image) {
                    if (image) {
                        vm.image = image;
                    } else {
                        vm.noImages = true;
                    }
                });
            }
        }]);
})();