﻿(function () {
    angular.module('app')
        .factory('imagesService', ['$q', '$http', function ($q, $http) {
            return {
                add: function (urls) {
                    var deferred = $q.defer();

                    $http.post("api/Images/Add", { urls: urls })
                        .then(
                            function (reponse) {
                                deferred.resolve(reponse.data);
                            },
                            function () {
                                deferred.reject();
                            }
                        );

                    return deferred.promise;
                },
                getRandom: function () {
                    var deferred = $q.defer();

                    $http.get("api/Images/GetRandom")
                        .then(
                            function (reponse) {
                                deferred.resolve(reponse.data);
                            },
                            function () {
                                deferred.reject();
                            }
                        );

                    return deferred.promise;
                },
                rate: function (rating) {
                    var deferred = $q.defer();

                    $http.put("api/Images/Rate", rating)
                        .then(
                            function (reponse) {
                                deferred.resolve(reponse.data);
                            },
                            function () {
                                deferred.reject();
                            }
                        );

                    return deferred.promise;
                }
            };
        }]);
})();