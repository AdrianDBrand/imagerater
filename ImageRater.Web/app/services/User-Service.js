﻿(function () {
    angular.module('app')
        .factory('userService', ['$q', '$http', function ($q, $http) {
            return {
                getId: function (nickName) {
                    var deferred = $q.defer();

                    $http.get("api/User/GetId/" + nickName)
                        .then(
                            function (reponse) {
                                deferred.resolve(reponse.data);
                            },
                            function () {
                                deferred.reject();
                            }
                        );

                    return deferred.promise;
                },
                getRatings: function (userId) {
                    var deferred = $q.defer();

                    $http.get("api/User/GetRatings/" + userId)
                        .then(
                            function (reponse) {
                                deferred.resolve(reponse.data);
                            },
                            function () {
                                deferred.reject();
                            }
                        );

                    return deferred.promise;
                }
            };
        }]);
})();