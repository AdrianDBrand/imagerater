﻿(function () {
    angular.module('app')
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider
                .state('images', {
                    url: '/images',
                    templateUrl: 'app/images/Images-View.html',
                    controller: 'imagesController as vm',
                    data: { pageTitle: 'Images' }
                });
        }]);
})();