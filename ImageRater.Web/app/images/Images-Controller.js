﻿(function () {
    angular.module('app')
        .controller('imagesController', ['appData', 'imagesService', '$state', function (appData, imagesService, $state) {
            var vm = this;

            vm.submit = function () {
                imagesService.add(vm.urls).then(function (id) {
                    $state.go('home');
                });
            };
        }]);
})();