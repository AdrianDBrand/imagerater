﻿(function () {
    angular.module('app')
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider
                .state('user', {
                    url: '/user',
                    templateUrl: 'app/user/User-View.html',
                    controller: 'userController as vm',
                    data: { pageTitle: 'User' }
                });
        }]);
})();