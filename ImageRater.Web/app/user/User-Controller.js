﻿(function () {
    angular.module('app')
        .controller('userController', ['appData', 'userService', '$state', function (appData, userService, $state) {
            var vm = this;

            vm.submit = function () {
                userService.getId(vm.nickName).then(function (id) {
                    appData.user = {
                        id: id,
                        nickName: vm.nickName
                    };
                    $state.go('home');
                });
            };
        }]);
})();