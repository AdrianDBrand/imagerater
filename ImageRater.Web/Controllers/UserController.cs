﻿using ImageRater.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ImageRater.Web.Controllers
{
    [RoutePrefix("api/User")]
    public class UserController : ApiController
    {
        ImageRaterDbContext dbContext;

        public UserController()
        {
            dbContext = new ImageRaterDbContext();
        }

        [Route("GetId/{nickName}")]
        [HttpGet]
        public int GetId(string nickName)
        {
            var user = dbContext.Users.SingleOrDefault(u => u.NickName == nickName);

            if (user == null)
            {
                user = new User { NickName = nickName };
                dbContext.Users.Add(user);
                dbContext.SaveChanges();
            }

            return user.Id;
        }

        [Route("GetRatings/{userId}")]
        [HttpGet]
        public object GetRatings(int userId)
        {
            return new {
                Liked = dbContext.ImageRatings.Where(ir => ir.UserId == userId && ir.Liked).Select(ir => ir.Image.Url),
                Disliked = dbContext.ImageRatings.Where(ir => ir.UserId == userId && !ir.Liked).Select(ir => ir.Image.Url)
            };
        }
    }
}
