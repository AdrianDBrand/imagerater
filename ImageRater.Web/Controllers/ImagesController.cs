﻿using ImageRater.Data;
using ImageRater.Web.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ImageRater.Web.Controllers
{
    [RoutePrefix("api/Images")]
    public class ImagesController : ApiController
    {
        ImageRaterDbContext dbContext;

        public ImagesController()
        {
            dbContext = new ImageRaterDbContext();
        }

        [Route("Add")]
        [HttpPost]
        public void Add(AddImagesRequest request)
        {
            var urls = request.Urls.Split('\n');

            var newImages = urls.Where(u => !dbContext.Images.Select(i => i.Url).Contains(u));

            foreach (var url in newImages)
            {
                if (!string.IsNullOrWhiteSpace(url))
                {
                    dbContext.Images.Add(new Image { Url = url });
                }
            }

            dbContext.SaveChanges();
        }

        [Route("GetRandom")]
        [HttpGet]
        public Image GetRandom()
        {
            var image = dbContext.Images.OrderBy(o => Guid.NewGuid()).FirstOrDefault();

            return image;
        }

        [Route("Rate")]
        [HttpPut]
        public void Rate(RateImageRequest request)
        {
            var rating = dbContext.ImageRatings.SingleOrDefault(ir => ir.ImageId == request.ImageId && ir.UserId == request.UserId);

            if (rating == null)
            {
                dbContext.ImageRatings.Add(new ImageRating { ImageId = request.ImageId, UserId = request.UserId, Liked = request.Liked });
            }
            else
            {
                rating.Liked = request.Liked;
            }
            dbContext.SaveChanges();
        }
    }
}
