﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImageRater.Web.Requests
{
    public class RateImageRequest
    {
        public int ImageId { get; set; }
        public int UserId { get; set; }
        public bool Liked { get; set; }
    }
}