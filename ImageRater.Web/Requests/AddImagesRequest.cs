﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImageRater.Web.Requests
{
    public class AddImagesRequest
    {
        public string Urls { get; set; }
    }
}